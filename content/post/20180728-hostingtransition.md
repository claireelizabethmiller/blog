+++
title = "Hosting Transition"
description = ""
tags = [
  "thebeginning"
]
date = "2018-07-28"
categories = [
    "prebirth",
]
+++

## Transitioning to new hosting!

In preparation for hosting more pictures and another content, the blog has
been moved to using Netlify for hosting and Gitlab for source content hosting.
Don't worry...this will be the last "techy" blog post for a long while :-)

- Steve

